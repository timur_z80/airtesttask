<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'EventsController@index']);
Route::get('/event/{id}', ['uses' => 'EventsController@show', 'as' => 'event.show']);
Route::post('/event/blogger/up', ['uses' => 'EventsController@moveEventBloggerUp', 'as' => 'event.blogger.move.up']);
Route::post('/event/blogger/down', ['uses' => 'EventsController@moveEventBloggerDown', 'as' => 'event.blogger.move.down']);
Route::post('/event/{id}/add-blogger', ['uses' => 'EventsController@addBloggerToEvent', 'as' => 'event.blogger.add']);
Route::post('/event/{id}/remove-blogger', ['uses' => 'EventsController@removeBloggerFromEvent', 'as' => 'event.blogger.remove']);