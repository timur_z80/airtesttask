<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBloggerEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogger_event', function (Blueprint $table) {
            $table->unsignedTinyInteger('blogger_id');
            $table->unsignedTinyInteger('event_id');
            $table->unsignedTinyInteger('blogger_order_number');

            $table->unique(['blogger_id', 'event_id', 'blogger_order_number']);

            $table->foreign('blogger_id')
                ->references('id')
                ->on('bloggers')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogger_event');
    }
}
