<?php

use App\Blogger;
use App\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsBloggersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->clearAllTables();

        $eventIds = $this->seedFakeModelItemsReturnIds(
            Event::class,
            15,
            15
        );

        $bloggersIds = $this->seedFakeModelItemsReturnIds(
            Blogger::class,
            5,
            10
        );

        foreach ($eventIds as $id) {
            $this->bindRandomBloggersToEvent($bloggersIds, $id);
        }

    }

    private function seedFakeModelItemsReturnIds(string $modelName, $min = 1, $max = 10): array
    {
        $faker = Faker\Factory::create();
        $items = factory($modelName, $faker->numberBetween($min, $max))->create();

        $ids = [];

        foreach ($items as $item) {
            $item->save();
            $ids[] = $item->id;
        }

        return $ids;
    }

    private function clearAllTables()
    {
        DB::table('blogger_event')->delete();
        DB::table('bloggers')->truncate();
        DB::table('events')->truncate();
    }

    private function bindRandomBloggersToEvent(array $bloggersIds, int $eventId)
    {
        $takeNumberOfBloggers = random_int(1, count($bloggersIds));
        $selectedIds = array_random($bloggersIds, $takeNumberOfBloggers);
        shuffle($selectedIds);

        foreach ($selectedIds as $key => $id) {
            DB::table('blogger_event')->insert( [
                'event_id' => $eventId,
                'blogger_id' => $id,
                'blogger_order_number' => $key + 1
            ]);
        }

    }
}
