<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Blogger::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'avatar' => $faker->imageUrl(50, 50),
    ];
});
