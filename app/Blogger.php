<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogger extends Model
{
    public $timestamps = false;

    public function events()
    {
        return $this->belongsToMany(Event::class, 'blogger_event');
    }
}
