<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Repositories\BloggerRepository;
use Repositories\EventRepository;

class EventsController extends Controller
{
    public function index()
    {
        $events = EventRepository::getAllEvents();
        $events = $this->rearrangeByYearAndMonth($events);

        return view('events.index', ['events' => $events]);
    }

    public function show(int $id)
    {
        $event = EventRepository::getEventWithBloggersById($id);
        $otherBloggers = BloggerRepository::getBloggersNotInEvent($id);

        return view('events.show', compact('event', 'otherBloggers'));
    }

    public function moveEventBloggerUp(Request $request)
    {
        $callback = function (int $eventId, int $orderNumber) {
            EventRepository::moveBloggerUp($eventId, $orderNumber);
        };

        return $this->runReorderGetResult($callback, $request);
    }

    public function moveEventBloggerDown(Request $request)
    {
        $callback = function (int $eventId, int $orderNumber) {
            EventRepository::moveBloggerDown($eventId, $orderNumber);
        };

        return $this->runReorderGetResult($callback, $request);
    }

    public function addBloggerToEvent(Request $request, int $eventId)
    {
        $request->validate([
            'blogger_id' => 'required|integer|exists:bloggers,id',
        ]);

        $blogger_id = $request->get('blogger_id');

        EventRepository::addBloggerToEvent($blogger_id, $eventId);

        return back();
    }

    public function removeBloggerFromEvent(Request $request, int $eventId)
    {
        $request->validate([
            'blogger_id' => 'required|integer|exists:bloggers,id',
        ]);

        $blogger_id = $request->get('blogger_id');

        EventRepository::removeBloggerFromEvent($blogger_id, $eventId);

        return back();
    }

    private function runReorderGetResult(callable $func, Request $request)
    {
        $request->validate([
            'event_id' => 'required|integer',
            'order_number' => 'required|integer',
        ]);

        try{
            $func(
                $request->get('event_id'),
                $request->get('order_number')
            );
        }catch (\Error | \Exception $e) {
            return back()->withErrors($e->getMessage());
        }

        return back();
    }

    private function rearrangeByYearAndMonth(array $events): array
    {
        $years = $this->sortArrayByKey($events, 'year');

        $result = [];

        foreach ($years as $year => $events) {
            $result[$year] = $this->reformatMonthNumToName(
                $this->sortArrayByKey($events, 'month')
            );
        }

        return $result;
    }

    private function sortArrayByKey(array $array, string $keyName): array
    {
        $result = [];

        foreach ($array as $key => $item) {
            $result[$item[$keyName]][$key] = $item;
        }

        ksort($result, SORT_REGULAR);

        return $result;
    }

    private function reformatMonthNumToName(array $events): array
    {
        $result = [];

        foreach ($events as $monthNum => $items) {
            $date = Carbon::createFromFormat('m', $monthNum);
            $month = mb_convert_case(
                $date->formatLocalized('%B'),
                MB_CASE_TITLE,
                "UTF-8"
            );
            $result[$month] = $items;
        }

        return $result;
    }
}
