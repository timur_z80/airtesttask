<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $timestamps = false;

    public function bloggers()
    {
        return $this->belongsToMany(Blogger::class, 'blogger_event')
                ->orderBy('blogger_order_number', 'asc')
                ->withPivot('blogger_order_number');
    }
}
