<?php
namespace Repositories;

use App\Event as EventModel;
use Illuminate\Support\Facades\DB;

class EventRepository
{
    public static function getAllEvents(): array
    {
        //для sqlite пришлось использовать strftime, но он не умеет форматировать в названия
        return EventModel::select(
                DB::raw('strftime("%Y", date) year, strftime("%m", date) month'),
                DB::raw('id, name, date')
            )
            ->groupBy('year', 'month', 'id')
            ->orderBy('date')
            ->get()
            ->toArray();
    }

    public static function getEventWithBloggersById(int $eventId): array
    {
        $event = EventModel::with('bloggers')
                    ->findOrFail($eventId);

        return $event->toArray();
    }

    public static function moveBloggerUp(int $eventId, int $orderNumber)
    {
        self::switchEventBloggersOrder($eventId, $orderNumber, -1);

    }

    public static function moveBloggerDown(int $eventId, int $orderNumber)
    {
        self::switchEventBloggersOrder($eventId, $orderNumber, 1);

    }

    public static function addBloggerToEvent(int $bloggerId, int $eventId)
    {
        $count = EventModel::findOrFail($eventId)
                    ->bloggers()
                    ->count();

        EventModel::findOrFail($eventId)
            ->bloggers()
            ->attach($bloggerId, ['blogger_order_number' => $count + 1]);

    }

    public static function removeBloggerFromEvent(int $bloggerId, int $eventId)
    {
        EventModel::findOrFail($eventId)
                ->bloggers()
                ->detach($bloggerId);

        self::updateBloggersOrder($eventId, self::getBloggersForEvent($eventId));
    }

    private static function switchEventBloggersOrder(int $eventId, int $orderNumber, int $index)
    {
        $bloggers = self::getBloggersForEvent($eventId);

        $from = $orderNumber;
        $to = $from + $index;

        [$bloggers[$from], $bloggers[$to]] = [$bloggers[$to], $bloggers[$from]];

        self::updateBloggersOrder($eventId, $bloggers);
    }

    private static function updateBloggersOrder(int $eventId, array $bloggers)
    {
        $updateData = [];

        foreach ($bloggers as $key => $blogger) {
            $updateData[$blogger['id']] = ['blogger_order_number' => $key + 1];
        }

        EventModel::findOrFail($eventId)
            ->bloggers()
            ->sync($updateData);
    }

    private static function getBloggersForEvent(int $eventId): array
    {
        return EventModel::findOrFail($eventId)
            ->bloggers()
            ->get()
            ->toArray();
    }
}