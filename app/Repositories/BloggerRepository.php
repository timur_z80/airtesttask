<?php


namespace Repositories;


use App\Blogger;

class BloggerRepository
{
    public static function getBloggersNotInEvent(int $eventId):array
    {
        return Blogger::whereNotIn('id', function ($q) use($eventId) {
                $q->select('blogger_id')
                    ->from('blogger_event')
                    ->where('event_id', $eventId);
            })
            ->get()
            ->toArray();
    }
}