<h1>Events</h1>
@foreach($events as $year => $items)
    @foreach($items as $month => $events)
        <h3>{{$month}} {{$year}}</h3>
        @foreach($events as $event)
            <div>
                <a href="{{route('event.show', $event['id'])}}">{{$event['name']}}</a>
                {{$event['date']}}
            </div>
        @endforeach
    @endforeach
@endforeach