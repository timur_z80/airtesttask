@php
    $bloggersCount = count($event['bloggers']);
@endphp

<div>
    @if(!$errors->isEmpty())
        <div id="errors">
            @foreach ($errors->all(':message') as $input_error)
                {{ $input_error }}
            @endforeach
        </div>
    @endif
    <h2>Event</h2>
    <div>Name: {{$event['name']}}</div>
    <div>Date: {{$event['date']}}</div>

    <div>

        <form method="post" action="{{route('event.blogger.add', $event['id'])}}">
            {{csrf_field()}}
            <h4>Add More Bloggers</h4>
            <select name="blogger_id">
                <option value="">--select--</option>
                @foreach($otherBloggers as $blogger)
                    <option value="{{$blogger['id']}}">{{$blogger['name']}}</option>
                @endforeach
            </select>
            <button>Add</button>
        </form>

        <h3>Bloggers</h3>
        @foreach($event['bloggers'] as $key => $blogger)
            <div>
                {{$blogger['pivot']['blogger_order_number']}}
                <img src="{{$blogger['avatar']}}" width="50px">
                {{$blogger['name']}}

                <form method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="blogger_id" value="{{$blogger['id']}}">
                    <input type="hidden" name="event_id" value="{{$event['id']}}">
                    <input type="hidden" name="order_number" value="{{$key}}">

                    @if($key > 0)
                        <button formaction="{{route('event.blogger.move.up')}}">Up</button>
                    @endif
                    @if($key < ($bloggersCount - 1))
                        <button formaction="{{route('event.blogger.move.down')}}">Down</button>
                    @endif
                    <button formaction="{{route('event.blogger.remove', $event['id'])}}">Remove</button>
                </form>

            </div>
        @endforeach
    </div>
</div>